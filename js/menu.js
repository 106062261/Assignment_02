var menu = {
    create: function(){
        game.stage.backgroundColor = "#938ec4";

        this.Title = game.add.text(game.width/2, game.height/3, "打飛機", {font:'30px',fill:'black'});
        this.Title.anchor.setTo(0.5,0.5);

        this.startBtn = game.add.button(game.width/2, game.height*4/8, "button", ()=>{this.start()}, this, 1, 0, 1);
        this.startBtn.anchor.setTo(0.5,0.5);
        this.settingBtn = game.add.button(game.width/2, game.height*5/8, "button", ()=>{this.setting()}, this, 3, 2, 3);
        this.settingBtn.anchor.setTo(0.5,0.5);
        // this.rankBtn = game.add.button(game.width/2, game.height*6/8, "button", ()=>{this.rank()}, this, 5, 4, 5);
        // this.rankBtn.anchor.setTo(0.5,0.5);

    },

    update: function(){

    },

    start: function(){
        game.btnAudio.play();
        game.state.start('gamemode');
    },

    setting: function(){
        game.btnAudio.play();
        game.state.start('setting');
    },

    rank: function(){
        game.btnAudio.play();
        game.state.start('rank');
    }
}

var setting = {
    create: function(){
        this.Title = game.add.text(game.width/2, game.height/3, "音量", {font:'30px',fill:'black'});
        this.Title.anchor.setTo(0.5,0.5);
        this.volume = game.add.text(game.width/2, game.height/2, game.volume.toString(), {font:'30px',fill:'black'});
        this.volume.anchor.setTo(0.5,0.5);

        this.backBtn = game.add.button(game.width/2, game.height*6/8, "button", ()=>{
            game.state.start('menu');
            game.btnAudio.play();
        }, this, 9, 8, 9);
        this.backBtn.anchor.setTo(0.5,0.5);

        this.upBtn = game.add.button(game.width*6/8, game.height/2, "updown", ()=>{
            if(game.volume < 100){
                game.volume += 10;
                game.bgm.volume = game.volume/100;
                game.btnAudio.volume = game.volume/100;
                game.shotAudi.volume = game.volume/100;
            }
            this.volume.setText(game.volume.toString());
            game.btnAudio.play();
        }, this, 1,0,1);
        this.upBtn.anchor.setTo(0.5,0.5);


        this.downBtn = game.add.button(game.width*2/8, game.height/2, "updown", ()=>{
            if(game.volume > 0){
                game.volume -= 10;
                game.bgm.volume = game.volume/100;
                game.btnAudio.volume = game.volume/100;
                game.shotAudi.volume = game.volume/100;
            }
            this.volume.setText(game.volume.toString());
            game.btnAudio.play();
        }, this, 3,2,3);
        this.downBtn.anchor.setTo(0.5,0.5);
    },

    update: function(){

    }

}

var rank = {
    create: function(){

    },

    update: function(){
        
    }
}

var endGame = {
    create: function(){
        game.stage.backgroundColor = "#938ec4";

        this.Title = game.add.text(game.width/2, game.height/3, "你的分數: " + game.scores, {font:'30px',fill:'black'});
        this.Title.anchor.setTo(0.5,0.5);

        this.backBtn = game.add.button(
            game.width/2, 
            game.height* 6/10, 
            'button',
            ()=>{
                game.state.start('menu');
                game.btnAudio.play();
            },
            this, 9, 8, 9
        );

        this.backBtn.anchor.setTo(0.5,0.5);
    },

    update: function(){
        
    }
}

var gamemode = {
    create: function(){
        game.stage.backgroundColor = "#938ec4";

        this.singleBtn = game.add.button(
            game.width/2, 
            game.height* 4/10, 
            'button',
            ()=>{
                game.gamemode = 1;
                game.state.start('play');
                game.btnAudio.play();
            },
            this, 11, 10, 11
        );

        this.singleBtn.anchor.setTo(0.5,0.5);


        this.multiBtn = game.add.button(
            game.width/2, 
            game.height* 6/10, 
            'button',
            ()=>{
                game.gamemode = 2;
                game.state.start('play');
                game.btnAudio.play();
            },
            this, 13, 12, 13
        );

        this.multiBtn.anchor.setTo(0.5,0.5);
    },

    update: function(){
        
    }
}