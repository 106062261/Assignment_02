var game = new Phaser.Game(300 , 600, Phaser.Auto);

$(document).ready(()=>{
    game.state.add("loading", load);
    game.state.add("menu", menu);
    game.state.add("setting", setting);
    game.state.add("rank", rank);
    game.state.add("gamemode", gamemode);
    game.state.add("play", play);
    game.state.add("endGame", endGame);


    game.state.start("loading");
})
